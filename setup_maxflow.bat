git clone https://github.com/pmneila/PyMaxflow.git
cd PyMaxflow
python setup.py build -c mingw32
python setup.py test
python setup.py bdist_wheel
copy dist\*.whl ..\
python setup.py install
pause
