# README #

NOTE:  To look at the notebooks online you can use the nbviewer on the 'raw' source files...
 * http://nbviewer.jupyter.org/urls/bitbucket.org/jfemiani/dfc2016/raw/3a13fd3f8792e9697c30e2f9a9afd513b9119a1d/nb/extract_from_video.ipynb
 * http://nbviewer.jupyter.org/urls/bitbucket.org/jfemiani/dfc2016/raw/3a13fd3f8792e9697c30e2f9a9afd513b9119a1d/nb/extract_from_shapefile.ipynb

There are some handy browser plugins to open nbviewer described at http://jiffyclub.github.io/open-in-nbviewer/ 


### What is this repository for? ###

This is work towards a paper based on the data provided in the DFC2016 contest.  Our group did not have time to put together a submission to the contest but we were inspired by the data. 


### How do I get set up? ###

To run these notebook you need python2.7, jupyter, and avariety of python packages. You can tell which packages are needed by looking at the imports on each ipython notebook.  If you are on windows it can be tricky to set those up, but it is possible with conda and the unnoficial python  binaries at http://www.lfd.uci.edu/~gohlke/pythonlibs/ 

This repo is not REALLY meant for production use by a large number of people -- it is more guidance and support for a paper we hope to produce... eventually :)

### Contribution guidelines ###

You should FORK this repo and send me PULL requests. 

### Who do I talk to? ###

Talk to me (the repo admin).